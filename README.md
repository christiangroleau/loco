# loco

![Release](https://img.shields.io/badge/Release-1.0-blue?labelColor=grey&style=flat)
![Node](https://img.shields.io/badge/Node-17.5+-blue?labelColor=grey&style=flat)
![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)

The crazy simple HTML + SASS + Gulp sandbox

A straightforward way to quickly prototype a web design or create a small static site

Supports the following features:

- Converts SASS to CSS
- Automatic browser refresh
- No bundler

## Getting started

```
npm install
npm run watch
```

Now, go to http://localhost:3000/
